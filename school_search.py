import csv, time, itertools

FNAME = 'school_data.csv'
STATE_CODES = states = { # FROM activestate.com
        'AK': 'Alaska',
        'AL': 'Alabama',
        'AR': 'Arkansas',
        'AS': 'American Samoa',
        'AZ': 'Arizona',
        'CA': 'California',
        'CO': 'Colorado',
        'CT': 'Connecticut',
        'DC': 'District of Columbia',
        'DE': 'Delaware',
        'FL': 'Florida',
        'GA': 'Georgia',
        'GU': 'Guam',
        'HI': 'Hawaii',
        'IA': 'Iowa',
        'ID': 'Idaho',
        'IL': 'Illinois',
        'IN': 'Indiana',
        'KS': 'Kansas',
        'KY': 'Kentucky',
        'LA': 'Louisiana',
        'MA': 'Massachusetts',
        'MD': 'Maryland',
        'ME': 'Maine',
        'MI': 'Michigan',
        'MN': 'Minnesota',
        'MO': 'Missouri',
        'MP': 'Northern Mariana Islands',
        'MS': 'Mississippi',
        'MT': 'Montana',
        'NA': 'National',
        'NC': 'North Carolina',
        'ND': 'North Dakota',
        'NE': 'Nebraska',
        'NH': 'New Hampshire',
        'NJ': 'New Jersey',
        'NM': 'New Mexico',
        'NV': 'Nevada',
        'NY': 'New York',
        'OH': 'Ohio',
        'OK': 'Oklahoma',
        'OR': 'Oregon',
        'PA': 'Pennsylvania',
        'PR': 'Puerto Rico',
        'RI': 'Rhode Island',
        'SC': 'South Carolina',
        'SD': 'South Dakota',
        'TN': 'Tennessee',
        'TX': 'Texas',
        'UT': 'Utah',
        'VA': 'Virginia',
        'VI': 'Virgin Islands',
        'VT': 'Vermont',
        'WA': 'Washington',
        'WI': 'Wisconsin',
        'WV': 'West Virginia',
        'WY': 'Wyoming'
}

school_db = {}
with open(FNAME, encoding = "ISO-8859-1") as f: 
    rdr = csv.DictReader(f)
    for i, row in enumerate(rdr):
        # tokens_db
        schoolname_tokens = set(row['SCHNAM05'].upper().split())
        city_tokens = set(row['LCITY05'].upper().split())
        statename_tokens = set(STATE_CODES.get(row['LSTATE05'], row['LSTATE05']).upper().split())
        school_db[i] = {
            'name_tokens': schoolname_tokens,
            'city_tokens': city_tokens,
            'state_tokens': statename_tokens,
            'info': row['SCHNAM05'] + '\n' + row['LCITY05'] + ', ' + row['LSTATE05']
        }


def tokenize(qry):
    tokens = qry.upper().split()
    return set(tokens)


def match_score(qry_tokens, school):
    if len(qry_tokens) < 1:
        return 0
    name_pts = len(qry_tokens.intersection(school_db[school]['name_tokens'])) / len(qry_tokens)
    city_pts = len(qry_tokens.intersection(school_db[school]['city_tokens'])) / len(qry_tokens)
    state_pts = len(qry_tokens.intersection(school_db[school]['state_tokens'])) / len(qry_tokens)
    return 15 * name_pts + 10 * city_pts + 5 * state_pts


def search(q):
    qry_tokens = tokenize(q)
    maxscore_1, maxscore_2, maxscore_3 = 0, 0, 0
    top_match_1, top_match_2, top_match_3 = '', '', ''
    for school_id in school_db:
        # get keywords for qry and school
        score = match_score(qry_tokens, school_id)
        if score > maxscore_1:
            top_match_3, maxscore_3 = top_match_2, maxscore_2
            top_match_2, maxscore_2 = top_match_1, maxscore_1
            maxscore_1 = score
            top_match_1 = school_db[school_id]['info']
        elif score > maxscore_2:
            top_match_3, maxscore_3 = top_match_2, maxscore_2
            maxscore_2 = score
            top_match_2 = school_db[school_id]['info']
        elif score > maxscore_3:
            maxscore_3 = score
            top_match_3 = school_db[school_id]['info']
    return top_match_1, top_match_2, top_match_3


def search_schools(q):
    start = time.time()
    top_match_1, top_match_2, top_match_3 = search(q)
    end = time.time()
    print('Results for "elementary school highland park"', f"search took {end - start:.4f}s")
    print('1.', top_match_1)  
    print('2.', top_match_2)    
    print('3.', top_match_3)