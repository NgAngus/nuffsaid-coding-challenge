import csv

FNAME = 'school_data.csv'

STATE_DICT = {} # state : set(schools)
METRO_DICT = {} # metro : set(schools)
CITY_DICT = {} # city : set(schools)
SCHOOL_SET = set()
MAX_CITY = ''

def load_data():
    global MAX_CITY
    with open(FNAME, encoding = "ISO-8859-1") as f: 
        rdr = csv.DictReader(f)
        for row in rdr:
            school = row['SCHNAM05']
            SCHOOL_SET.add(school)
            # states
            if not STATE_DICT.get(row['LSTATE05'], False):
                STATE_DICT[row['LSTATE05']] = {school}
            else:
                STATE_DICT[row['LSTATE05']].add(school)
            # metros
            if not METRO_DICT.get(row['MLOCALE'], False):
                METRO_DICT[row['MLOCALE']] = {school}
            else:
                METRO_DICT[row['MLOCALE']].add(school)
            # cities
            if not CITY_DICT.get(row['LCITY05'], False):
                CITY_DICT[row['LCITY05']] = {school}
            else:
                CITY_DICT[row['LCITY05']].add(school)
            MAX_CITY = max(MAX_CITY, row['LCITY05'], key=lambda x: len(CITY_DICT.get(x, set())))


def print_counts():
    load_data()
    print("Total Schools:", len(SCHOOL_SET))
    print("Schools by State:")
    for state in STATE_DICT:
        print(state + ":", len(STATE_DICT[state]))
    print('Schools by Metro-centric locale:')
    for metro in METRO_DICT:
        print(metro + ":", len(METRO_DICT[metro]))
    print('City with most schools:', MAX_CITY, f"({len(CITY_DICT[MAX_CITY])})")
    print('Unique cities with at least one school:', len(CITY_DICT))

